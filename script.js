const tabsContainer = document.querySelector('.centered-content');

const tabsList = tabsContainer.querySelector('.tabs');

const tabsContentList = tabsContainer.querySelector('.tabs-content');

function showTabContent(tabNum) {
    const tabContentItems = tabsContentList.querySelectorAll('li');
    tabContentItems.forEach(item => item.style.display = 'none');

    const selectedContentItem = tabsContentList.querySelector(`li[data-textnum="${tabNum}"]`);
    if (selectedContentItem) {
        selectedContentItem.style.display = 'block';
    }
}

tabsList.addEventListener('click', (event) => {
    if (event.target.classList.contains('tabs-title')) {
        const tabNum = event.target.dataset.num;

        const tabs = tabsList.querySelectorAll('.tabs-title');
        tabs.forEach(tab => tab.classList.remove('active'));

        event.target.classList.add('active');

        showTabContent(tabNum);
    }
});

const firstTab = tabsList.querySelector('.tabs-title');
if (firstTab) {
    firstTab.classList.add('active');
    showTabContent(firstTab.dataset.num);
}