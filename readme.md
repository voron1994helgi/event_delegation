 Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
Цей метод скидає дефолтні налаштування поведінки браузера . 
2. В чому сенс прийому делегування подій?
Сенс у  тому , щоб не навішувати окрему подію на кожен дочірній елемент ,а в тому ,щоб повісити слухач події  на батьківський елемент  . 
3. Які ви знаєте основні події документу та вікна браузера? 
DOMContentLoaded - працює , коли загрузилось DOM-дерево , але без сторонніх ресурсів , таких як , наприклад , стилі або зображення . 
load - працює ,  коли всі ресурси були загружені
beforeunload -  працює , коли користувач хоче покинути сторінку ,можемо за допомогою цієї події перепитати користувача чи дійсно він хоче покинути сторінку .
unload - працює для того , щоб збирану інформацію  статистики дій користувача на сторінці відправити на сервер .